
FROM registry.gitlab.com/developers.ieduca/devops/compilador:latest AS build

WORKDIR  /home/app/

ARG main_name_project

COPY src /home/app/src
COPY pom.xml /home/app

RUN mvn -f /home/app/pom.xml clean package
RUN ls -la  /home/app/ && ls -la  /home/app/target && sleep 5
RUN rm -Rf /home/app/$main_name_project && mkdir /home/app/$main_name_project && ls -la  /home/app
#RUN mkdir /home/app/$main_name_project && ls -la  /home/app
RUN unzip "/home/app/target/$main_name_project.zip" -d /home/app/$main_name_project && ls -la  /home/app/$main_name_project && sleep 5



#
# Package stage
#
FROM emejiaieduca/mule:3.9.v2

ARG main_name_project

RUN mkdir /opt/mule/apps/$main_name_project

COPY --from=build /home/app/$main_name_project  /opt/mule/apps/$main_name_project

RUN ls -la  /opt/mule/apps
RUN ls -la /opt/mule/apps && sleep 5
